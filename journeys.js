#!/usr/bin/env node
'use strict';

const { resolve: resolvePath } = require('path');
const yargsParser = require('yargs-parser');
const options = process.argv.slice(2);
const cwd = process.cwd();
const { _: [ userJourneysPath ], url } = yargsParser(options);

const paint = require('./src/paint');
const journeyman = require('./src/journeys');

const userJourneys = resolvePath(cwd, userJourneysPath);
const journeys = require(userJourneys);

(async function() {      
    await journeyman(url).takeJourneys(journeys).then(paint);
}());