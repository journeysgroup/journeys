describe('tests orchestration of tests', () => {

    const { fn, mock  } = jest;

    const supertest = fn(url => url);
    const reportProvider = { reportOn: fn(input => Promise.resolve(input)) };
    const reports = fn(() => reportProvider);

    mock('supertest', () => supertest);
    mock('../src/reports', () => reports);

    const journeyman = require('../src/journeys');

    const journeys = {
        UserOne: [ 'journeyOne', 'journeyTwo' ],
        UserTwo: [ 'journeyOne', 'journeyTwo' ],
    };

    it('should combine all reports into object for output or further processing', done => {
        const url = 'url';
        journeyman(url).takeJourneys(journeys).then(result => {
            expect(result).toStrictEqual(journeys);

            done();
        });
    });

});
