describe('tests reults following successful response and failed corroboration', () => {

    const { fn, mock, clearAllMocks } = jest;

    afterEach(() => {
        clearAllMocks();
    });

    const badRequest = 'badRequest';
    const invalid = 'invalid';
    const success = 'success';
    const successPath = '/success';
    const failPath = '/fail';

    const succeed = { send: fn(() => Promise.resolve(success)) };
    const renounce = { send: fn(() => Promise.reject()) };
    const requester = fn(input => input === badRequest ? renounce : succeed);        
        
    const pass = { corroborate: fn(() => Promise.resolve()) };
    const fail = { corroborate: fn(() => Promise.reject()) };
    const responder = fn(input => input.url === successPath ? pass : fail);
    
    mock('../src/request', () => requester);
    mock('../src/response', () => responder);

    it(`should return results reflecting success`, done => {
        const resultsProvider = require('../src/results');
        
        const validate = fn();
        const then = fn();
        const url = successPath;
        const status = 200;
        const test = { status, then, url, validate };
        
        resultsProvider(success).getResultsOf(test).then(result => {
            expect(result.complete).toBe(true);
            expect(result.response).toBe(success);
            expect(result.url).toBe(url);
            expect(result.status).toBe(status);

            done();    
        })
    });
    
    it(`should return results reflecting corroboration failure`, done => {
        const resultsProvider = require('../src/results');
        
        const validate = fn();
        const then = fn();
        const url = failPath;
        const status = 200;
        const test = { status, then, url, validate };
        
        resultsProvider(invalid).getResultsOf(test).then(result => {
            expect(result.complete).toBe(false);
            expect(result.response).toBe(success);
            expect(result.url).toBe(url);
            expect(result.status).toBe(status);

            done();    
        })
    });

    it(`should return results reflecting failed response`, done => {
        const resultsProvider = require('../src/results');
        
        const validate = fn();
        const then = fn();
        const url = '/path';
        const status = 200;
        const test = { status, then, url, validate };
        
        resultsProvider(badRequest).getResultsOf(test).then(result => {
            expect(result.complete).toBe(false);
            expect(result.response).toBeFalsy();
            expect(result.url).toBe(url);
            expect(result.status).toBe(status);

            done();    
        })
    });
});