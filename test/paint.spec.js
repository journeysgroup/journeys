describe('tests console output of results', () => {

    const { fn, mock, spyOn } = jest;

    const chalk = {
        bold: {
            blue: fn(input => input),
            green: fn(input => input),
            red: fn(input => input),
        }
    }

    mock('chalk', () => chalk);

    const userOneFirstDescription = 'UserOneFirstTest';
    const userOneSecondDescription = 'UserOneSecondTest';
    const userTwoFirstDescription = 'UserTwoFirstTest';
    const userTwoSecondDescription = 'UserTwoSecondTest';

    const userOneError = 'userOneError';
    const userTwoError = 'userTwoError';

    const results = {
        UserOne: [
            {
                complete: true,
                description: userOneFirstDescription
            },
            {
                complete: false,
                description: userOneSecondDescription,
                error: userOneError
            }
        ],
        UserTwo: [
            {
                complete: false,
                description: userTwoFirstDescription,
                error: userTwoError
            },
            {
                complete: true,
                description: userTwoSecondDescription
            }
        ]
    };

    const paint = require('../src/paint');
    const tick = ' \u2713';
    const cross = ' \u2717';

    it('should send the correct reports to the console', () => {
        spyOn(console, 'log');

        paint(results);

        expect(console.log).toHaveBeenCalledTimes(9);
        expect(console.log).toHaveBeenNthCalledWith(1, '\nUser Journey of UserOne:');
        expect(console.log).toHaveBeenNthCalledWith(2, `${tick} ${userOneFirstDescription}`);
        expect(console.log).toHaveBeenNthCalledWith(3, `${cross} ${userOneSecondDescription}`);
        expect(console.log).toHaveBeenNthCalledWith(4, `\n${userOneError}\n`);
        expect(console.log).toHaveBeenNthCalledWith(5, '\nUser Journey of UserTwo:');
        expect(console.log).toHaveBeenNthCalledWith(6, `${cross} ${userTwoFirstDescription}`);
        expect(console.log).toHaveBeenNthCalledWith(7, `\n${userTwoError}\n`);
        expect(console.log).toHaveBeenNthCalledWith(8, `${tick} ${userTwoSecondDescription}`);
        expect(console.log).toHaveBeenNthCalledWith(9, '\n');
    });

});