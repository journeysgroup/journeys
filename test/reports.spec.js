describe('tests building of reports from user journeys', () => {
    
    const { fn, mock, clearAllMocks } = jest;

    afterEach(() => {
        clearAllMocks();
    });

    const base = 'base';

    const first = { description: 'first' };
    const second = { description: 'second' };
    const third = { description: 'third' };
    const fourth = { description: 'fourth '};

    const journey = [ first, second, third, fourth ];
    const resultProvider = { getResultsOf: fn(input => Promise.resolve(input)) };
    const results = fn(() => resultProvider);

    mock('../src/results', () => results);

    const reportsProvider = require('../src/reports');

    it(`should produce info line then report all journeys as successes except the second to last`, done => {
        const reports = reportsProvider(base);
        reports.reportOn(journey).then(([ firstReport, secondReport, thirdReport, fourthReport ]) => {
            expect(firstReport).toBe(first);
            expect(secondReport).toBe(second);
            expect(thirdReport).toBe(third);
            expect(fourthReport).toBe(fourth);
            done();
        });
    });

});