describe('tests supported HTTP methods', () => {

    const { DELETE, GET, POST, PUT } = require('../src/methods');

    const data = 'data';
    const expected = { body: 'body' };

    const { fn } = jest;

    it('should make a DELETE request to the given url using the supplied base', () => {
        const url = '/url';
        const base = { delete: fn(() => expected) };
        
        const actual = DELETE(base, { url });

        expect(actual).toBe(expected);
        expect(base.delete).toHaveBeenCalledWith(url);
    });

    it('should make a GET request to the given url using the supplied base', () => {
        const url = '/url';
        const base = { get: fn(() => expected) };
        
        const actual = GET(base, { url });

        expect(actual).toBe(expected);
        expect(base.get).toHaveBeenCalledWith(url);
    });

    it('should make a POST request to the given url using the supplied base and sending the provided data', () => {
        const url = '/url';
        const send = { send: fn(() => expected) };
        const base = { post: fn(() => send) };
        
        const actual = POST(base, { send: data, url });

        expect(actual).toBe(expected);
        expect(base.post).toHaveBeenCalledWith(url);
        expect(send.send).toHaveBeenCalledWith(data);
    });

    it('should make a PUT request to the given url using the supplied base and sending the provided data', () => {
        const url = '/url';
        const send = { send: fn(() => expected) };
        const base = { put: fn(() => send) };
        
        const actual = PUT(base, { send: data, url });

        expect(actual).toBe(expected);
        expect(base.put).toHaveBeenCalledWith(url);
        expect(send.send).toHaveBeenCalledWith(data);
    });

    it('should make a DELETE request to the supplied url using the supplied base', () => {
        const url = '/url';
        const base = { delete: fn(() => expected) };
        
        const actual = DELETE(base, { url: () => url });

        expect(actual).toBe(expected);
        expect(base.delete).toHaveBeenCalledWith(url);
    });

    it('should make a GET request to the supplied url using the supplied base', () => {
        const url = '/url';
        const base = { get: fn(() => expected) };
        
        const actual = GET(base, { url: () => url });

        expect(actual).toBe(expected);
        expect(base.get).toHaveBeenCalledWith(url);
    });

    it('should make a POST request to the supplied url using the supplied base and sending the supplied data', () => {
        const url = '/url';
        const send = { send: fn(() => expected) };
        const base = { post: fn(() => send) };
        
        const actual = POST(base, { send: () => data, url: () => url });

        expect(actual).toBe(expected);
        expect(base.post).toHaveBeenCalledWith(url);
        expect(send.send).toHaveBeenCalledWith(data);
    });

    it('should make a PUT request to the supplied url using the supplied base and sending the supplied data', () => {
        const url = '/url';
        const send = { send: fn(() => expected) };
        const base = { put: fn(() => send) };
        
        const actual = PUT(base, { send: () => data, url: () => url });

        expect(actual).toBe(expected);
        expect(base.put).toHaveBeenCalledWith(url);
        expect(send.send).toHaveBeenCalledWith(data);
    });

});