describe('tests response corroboration', () => {
    
    const { fn, clearAllMocks } = jest;

    afterEach(() => {
        clearAllMocks();
    });

    it(`should call validate and then methods when both exist`, done => {
        const then = fn();
        const validate = fn();
        const test = { validate, then };
        const response = 'response';
        const responder = require('../src/response')(test);
        responder.corroborate(response).then(() => {
            expect(validate).toHaveBeenCalledTimes(1);
            expect(validate).toHaveBeenCalledWith(response);

            expect(then).toHaveBeenCalledTimes(1);
            expect(then).toHaveBeenCalledWith(response);

            done();
        });
    });

    it(`should call only validate when then does not exist`, done => {
        const then = fn();
        const validate = fn();
        const test = { validate };
        const response = 'response';
        const responder = require('../src/response')(test);
        responder.corroborate(response).then(() => {
            expect(validate).toHaveBeenCalledTimes(1);
            expect(validate).toHaveBeenCalledWith(response);

            expect(then).not.toHaveBeenCalled();

            done();
        });
    });

    it(`should call only then when validate does not exist`, done => {
        const then = fn();
        const validate = fn();
        const test = { then };
        const response = 'response';
        const responder = require('../src/response')(test);
        responder.corroborate(response).then(() => {
            expect(then).toHaveBeenCalledTimes(1);
            expect(then).toHaveBeenCalledWith(response);

            expect(validate).not.toHaveBeenCalled();

            done();
        });
    });

    it(`should do nothing when neither validate nor then exist`, done => {
        const then = fn();
        const validate = fn();
        const test = {  };
        const response = 'response';
        const responder = require('../src/response')(test);
        responder.corroborate(response).then(() => {
            expect(validate).not.toHaveBeenCalled();
            expect(then).not.toHaveBeenCalled();

            done();
        });
    });

});