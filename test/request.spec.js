describe('tests building of requests from provided object', () => {
    
    const { fn, mock, clearAllMocks } = jest;

    afterEach(() => {
        clearAllMocks();
    });

    const status = 'status';
    const response = { body: 'body' };
    const base = { expect: fn(first => first === status ? Promise.resolve(response) : base), set: fn(() => base) };

    const methods = {
        DELETE: fn(() => base),
        GET: fn(() => base),
        POST: fn(() => base),
        PUT: fn(() => base)
    };

    mock('../src/methods', () => methods);

    it(`should correctly build the request according to the input schema`, done => {
        const description = 'description';
        const accept = 'application/json';
        const contentType = /json/;
        const method = 'GET';
        const url = 'url';

        const receive = { 'Content-Type' : contentType }; 
        const send = { Accept : accept };
        const headers = { send, receive };
        
        const requests = require('../src/request')(base);
        const request = { description, headers, method, status, url };

        requests.send(request).then(() => {
            expect(methods.GET).toHaveBeenCalledTimes(1);
            expect(methods.GET).toHaveBeenCalledWith(base, request);

            expect(base.set).toHaveBeenCalledTimes(1);
            expect(base.set).toHaveBeenCalledWith('Accept', accept);

            expect(base.expect).toHaveBeenCalledTimes(2);
            expect(base.expect).toHaveBeenNthCalledWith(1, 'Content-Type', contentType);
            expect(base.expect).toHaveBeenNthCalledWith(2, status);

            done();
        });
    });

    it(`should correctly build the request according to the input schema when using suppliers`, done => {
        const description = 'description';
        const accept = 'application/json';
        const contentType = /json/;
        const method = 'GET';
        const url = 'url';

        const receive = { 'Content-Type' : () => contentType }; 
        const send = { Accept : () => accept };
        const headers = { send, receive };
        
        const requests = require('../src/request')(base);
        const request = { description, headers, method, status, url };

        requests.send(request).then(() => {
            expect(methods.GET).toHaveBeenCalledTimes(1);
            expect(methods.GET).toHaveBeenCalledWith(base, request);

            expect(base.set).toHaveBeenCalledTimes(1);
            expect(base.set).toHaveBeenCalledWith('Accept', accept);

            expect(base.expect).toHaveBeenCalledTimes(2);
            expect(base.expect).toHaveBeenNthCalledWith(1, 'Content-Type', contentType);
            expect(base.expect).toHaveBeenNthCalledWith(2, status);

            done();
        });
    });

});