const requesters = require('./request');
const responder = require('./response');

module.exports = function(base) {
    const requester = requesters(base);
    
    function getResultsOf(test) {
        const { validate, then, ...request } = test;
        return requester.send(request).then(response => {
            return responder(test).corroborate(response).then(() => {
                return Object.assign(request, { complete: true,  response }); 
            }).catch(error => {
                return Object.assign(request, { complete: false,  response, error });
            });
        }).catch(error => {
            return Object.assign(request, { complete: false,  error });
        });
    }

    return { getResultsOf };
};