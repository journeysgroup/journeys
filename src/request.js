const methods = require('./methods');

module.exports = function(base) {
    function send(request) {
        function getBaseRequest() {
            function getKey({ method }) {
                return Object.keys(methods).find(name => {
                    return name === method;
                });
            }
            
            const key = getKey(request);
            return methods[key](base, request);
        }

        function setHeaders(request, send) {
           return Object.entries(send).reduce(( accumulator, [ name, value ] ) => {
                return accumulator.set(name, ( value instanceof Function ) ? value.call(request) : value);
            }, request); 
        }
    
        function setExpectations(request, receive) {
            return Object.entries(receive).reduce(( accumulator, [ name, value ] ) => {
                return accumulator.expect(name, ( value instanceof Function ) ? value.call(request) : value);
            }, request);    
        }
    
        function setStatus(request, status) {
            return request.expect(status);
        }
        
        const { headers: { send, receive }, status } = request;
        
        const baseRequest = getBaseRequest();
        const withHeaders = setHeaders(baseRequest, send);
        const withExpectations = setExpectations(withHeaders, receive);
        const withStatus = setStatus(withExpectations, status);
    
        return withStatus;
    }

    return { send };
};