const supertest = require('supertest');
const reporter = require('./reports');

module.exports = function(url) {
    const base = supertest(url);
    const reports = reporter(base);
    
    async function takeJourneys(journeys) {
        async function getResults() {
            return await Promise.all(Object.entries(journeys).map(async ([ user, journey ]) => {
                return { user, report: await reports.reportOn(journey) };
            }));
        }

        function generateReports(results) {
            return results.reduce((reports, { user, report }) => {
                return Object.assign(reports, { [ user ]: report });
            }, { });
        }

        const results = await getResults();
        return generateReports(results);
    }

    return { takeJourneys };
};