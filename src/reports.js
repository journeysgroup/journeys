const resultProvider = require('./results');

module.exports = function(base) {
    const results = resultProvider(base);

    function reportOn(journey) {
        return journey.reduce(async (accumulator, test) => {
            const reports = await accumulator;
            const report = await results.getResultsOf(test);
            return [ ...reports, report ];
        }, Promise.resolve([ ]));
    }

    return { reportOn };
    
};