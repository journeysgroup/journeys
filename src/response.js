const requests = require('./request');

module.exports = function(test) {
    const { validate, then } = test;

    async function corroborate(response) {
        if ( validate ) {
            validate.call(test, response);
        }
    
        if ( then ) {
            then.call(test, response);
        }
    }

    return { corroborate };
};