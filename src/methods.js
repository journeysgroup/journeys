module.exports = {
    DELETE: function(base, request) {
        const { url } = request;
        const endpoint = ( url instanceof Function ) ? url.call(request) : url;
        return base.delete(endpoint);
    },
    GET: function(base, request) {
        const { url } = request;
        const endpoint = ( url instanceof Function ) ? url.call(request) : url;
        return base.get(endpoint);
    },
    POST: function(base, request) {
        const { send, url } = request;
        const endpoint = ( url instanceof Function ) ? url.call(request) : url;
        const post = base.post(endpoint);
        const data = ( send instanceof Function ) ? send.call(post) : send;
        return post.send(data);    
    },
    PUT: function(base, request) {
        const { send, url } = request;
        const endpoint = ( url instanceof Function ) ? url.call(request) : url;
        const put = base.put(endpoint);
        const data = ( send instanceof Function ) ? send.call(put) : send;
        return put.send(data);
    }
};