const chalk = require('chalk');

const pass = chalk.bold.green;
const fail = chalk.bold.red;
const info = chalk.bold.blue;

const tick = ' \u2713';
const cross = ' \u2717';

module.exports = function(results) {
    for (const user in results) {
        console.log(info(`\nUser Journey of ${user}:`));
        for (const result of results[user]) {
            if ( result.complete ) {
                console.log(`${pass(tick)} ${result.description}`);
                continue;    
            }

            console.log(`${fail(cross)} ${result.description}`);
            console.log(`\n${result.error}\n`);
        }   
    }
    
    console.log('\n');
};