const expect = require('expect');

module.exports = {
    UserOne: [
        {
            description: `UserOne sends firstCall`,
            headers: {
                send: {
                    Accept : 'application/json'
                },
                receive: {
                    'Content-Type' : 'text/html; charset=utf-8'
                }        
            },
            method: 'GET',
            status: 200,
            url: `/firstCall`,
            validate: function(response) {
                expect(response.text).toBe('firstCall');
            }
        },
        {
            description: `UserOne sends secondCall`,
            headers: {
                send: {
                    Accept : 'application/json'
                },
                receive: {
                    'Content-Type' : 'text/html; charset=utf-8'
                }        
            },
            method: 'GET',
            status: 200,
            url: `/secondCall`,
            validate: function(response) {
                expect(response.text).toBe('secondCall');
            }
        },
        {
            description: `UserOne sends errorCall`,
            headers: {
                send: {
                    Accept : function() {
                        return 'application/json';
                    }
                },
                receive: {
                    'Content-Type' : function() {
                        return 'text/html; charset=utf-8';
                    }
                }        
            },
            method: 'GET',
            status: 200,
            url: function() {
                return '/';
            }
        },
        {
            description: `UserOne sends fourthCall`,
            headers: {
                send: {
                    Accept : 'application/json'
                },
                receive: {
                    'Content-Type' : 'text/html; charset=utf-8'
                }        
            },
            method: 'GET',
            status: 200,
            url: `/fourthCall`,
            validate: function(response) {
                expect(response.text).toBe('fourthCall');
            }
        }
    ],
    UserTwo: [
        {
            description: `UserTwo sends firstCall`,
            headers: {
                send: {
                    Accept : 'application/json'
                },
                receive: {
                    'Content-Type' : 'text/html; charset=utf-8'
                }        
            },
            method: 'GET',
            status: 200,
            url: `/firstCall`,
            validate: function(response) {
                expect(response.text).toBe('firstCall');
            }
        },
        {
            description: `UserTwo sends secondCall`,
            headers: {
                send: {
                    Accept : 'application/json'
                },
                receive: {
                    'Content-Type' : 'text/html; charset=utf-8'
                }        
            },
            method: 'GET',
            status: 200,
            url: `/secondCall`,
            validate: function(response) {
                expect(response.text).toBe('secondCall');
            }
        },
        {
            description: `UserTwo sends thirdCall`,
            headers: {
                send: {
                    Accept : 'application/json'
                },
                receive: {
                    'Content-Type' : 'text/html; charset=utf-8'
                }        
            },
            method: 'GET',
            status: 200,
            url: `/thirdCall`,
            validate: function(response) {
                expect(response.text).toBe('thirdCall');
            }
        },
        {
            description: `UserTwo sends errorCall`,
            headers: {
                send: {
                    Accept : 'application/json'
                },
                receive: {
                    'Content-Type' : 'text/html; charset=utf-8'
                }        
            },
            method: 'GET',
            status: 200,
            url: `/errorCall`,
            validate: function(response) {
                expect(response.text).toBeFalsy();
            }
        }
    ]
}