const expect = require('expect');
const { v4: uuidv4 } = require('uuid');
const numberOfSimultaneousUsers = 500;
const testsPerUser = 10;
const OK = 200;

module.exports = [ ...Array(numberOfSimultaneousUsers).keys() ].reduce(current => {
    const username = uuidv4();
    return Object.assign(current, { [ username ]:  [ ...Array(testsPerUser).keys() ].map(() => {
        const input = uuidv4();
        return {
            description: `${username} sends ${input}`,
            headers: {
                send: {
                    Accept : 'application/json'
                },
                receive: {
                    'Content-Type' : 'text/html; charset=utf-8'
                }        
            },
            method: 'GET',
            status: OK,
            url: `/${input}`,
            validate: function(response) {
                const random = Math.floor(Math.random() * 10) + 1;
                const expectation = random !== 10 ? input : 'showFailure';

                expect(response.text).toBe(expectation);
            },
            then: function(_) {
                console.log(`${username}'s call to ${this.description} completed...`);   
            }
        }
    })});
}, { });
