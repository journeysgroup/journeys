const express = require('express');
const app = express();
const port = 9001;

app.get('/:input', function(request, response) {
    response.send(request.params.input);
});

app.listen(port, () => console.log(`Reflect input server started on ${port}`));